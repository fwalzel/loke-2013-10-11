$.mvc.controller.create("main", {

    default : function () {},

    toggleBars : function ()
    {
        console.log("@ toggleBars()");
        $.ui.toggleHeaderMenu(false);
        $.ui.toggleNavMenu(false);
    },

    showhideMenu : function ()
    {
        console.log("@ showhideMenu()");
        var t = $("#menu");
        if (t.hasClass("out")) t.replaceClass("out", "in");
        else if (t.hasClass("in")) t.replaceClass("in", "out");
    },

    init : function ( ) {


        //$(".panelnav").on("click", this.toggleBars);
        // -- log
        console.log("@ contoller 'main' loaded.");
    }

});
