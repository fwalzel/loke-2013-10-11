$.mvc.controller.create("login", {

    views : ["views/login.js"],

    default : function () {

        console.log("@ login");

        $.ui.loadContent("#login",false,false,"up");
        $.ui.toggleHeaderMenu(false);
        $.ui.toggleNavMenu(false);
    },

    init : function ( ) {
        v.ui.showNav(false);

        var data = {
            usr : l.login.usr[v.ln],
            pwd : l.login.pwd[v.ln],
            loginbutton : l.login.loginbutton[v.ln]
        };
        $("#login div.inner").append($.template("views/login.js", data));



        // -- log
        console.log("@ controller 'login' loaded.");
    }

});

