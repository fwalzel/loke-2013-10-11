var UIhandler = function () { };


UIhandler.prototype.hideKeyboard = function () {
    document.activeElement.blur();
};

UIhandler.prototype.showNav = function (arbiter)
{
    if (arbiter)
        $.ui.enableSideMenu();
    else
        $.ui.disableSideMenu();
};


