/* -------------------------------- */
//     SNIPPETS EARLY TO LOAD       //
/* -------------------------------- */





/* -------------------------------- */
//       GET LANGUAGE FILES         //
/* -------------------------------- */

$.ajax({
    url: v.langJSON,
    async: false,
    dataType: 'json',
    success: function(data) { l = data; },
    error: function() { console.log('@ Could not load language file"' + v.langJSON + '".'); }
});



/* -------------------------------- */
//              INIT MVC            //
/* -------------------------------- */

v.app = new $.mvc.app();

// load MVC Models & Contollers
v.app.loadModels(["listview", "setloke"]);
v.app.loadControllers(["login", "main", "listview", "setloke"]);
v.app.setViewType("text/html");


/* -------------------------------- */
//         DEVICE IS READY          //
/* -------------------------------- */

document.addEventListener("deviceready",onDeviceReady,false);

// Define Sources for phonegap camera
function onDeviceReady() 
{
    // check whether app is running on mobile or not?
    v.onDevice = lo.isMobile();
    // Get language presets of device
    if (v.onDevice) lo.checkLanguage();
}


/* -------------------------------- */
//           UI IS READY            //
/* -------------------------------- */

$.ui.ready(function()
{
    new FastClick(document.body);
    $.ui.showBackbutton=false;
    $.ui.toggleHeaderMenu(false);
    $.ui.toggleNavMenu(false);
    //$("#menu").addClass("out");
});


