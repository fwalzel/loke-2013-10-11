var lo = (function () {

    // ----- DECLARATIONS ----- //


    // ----- PRIVATE FUNCTIONS ----- //


    // ----- FUNCTIONS ----- //
    var f = {

        /** isMobile
         * is app running on a mobile device?
         * @returns {boolean}
         */
        isMobile : function()
        {
            return ( ($.os.android || $.os.androidICS ) ||
                     ($.os.ipad  || $.os.iphone ) ) ? true : false;
        },

        checkLanguage : function ()
        {
            navigator.globalization.getPreferredLanguage(
                function (language) {
                    console.log("@ Device Language: " + language.value);
                    v.lang = language.value;
                },
                function () {alert('@ Error getting language\n');}
            );
        },


    /**
         *
         * @param onSuc
         * @param onErr
         * @param data
         * @param path
         * @param url
         * @param method
         * @param datatype
         */
        requestJSON : function (onSuc, onErr, data, path, url, method, datatype, async)
        {
            console.log("@ requestJSON()");

            var resChain = "/";
            if (_.isObject(path)) {
                for(var i=0; i<path.length; i++) {
                    resChain += path[i] + "/";
                }
                resChain = resChain.substring(0, resChain.length-1);
            }

            var params =
            {
                url :
                    url,
                data :
                    JSON.stringify(data),
                type :
                    _.isSet(method)? method : "POST",
                dataType :
                    _.isSet(datatype) ? datatype : "json",
                async :
                    _.isSet(async) ? async : true,
                crossDomain :
                    true,
                contentType :
                    "application/json",

                success : function(re){
                    if (_.isFunction(onSuc)) {
                        onSuc(re);
                    }
                    else if (typeof onSuc === "string") {
                        $.trigger(document, onSuc, [re]);
                    }
                    else {
                        console.log("@ requestJSON success. Server returns:");
                        console.log(re);
                    }
                    return true;
                },

                error : function (re) {
                    if (_.isFunction(onErr)) {
                        onErr(re);
                    }
                    else if (typeof onErr === "string"){
                        $.trigger(document, onErr, re);
                    }
                    else {
                        console.log("@ requestJSON caused error. Server returns:");
                        console.log(re);
                    }
                    return false;
                }
            };

            console.debug(params);

            $.ajax(params);
        }


    };

    return f;
})();












