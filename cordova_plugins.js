cordova.define('cordova/plugin_list', function(require, exports, module) {
    module.exports = [
        {
            "file": "plugins/GlobalizationError.js",
            "id": "org.apache.cordova.globalization.GlobalizationError",
            "clobbers": [
                "window.GlobalizationError"
            ]
        },
        {
            "file": "plugins/globalization.js",
            "id": "org.apache.cordova.globalization.globalization",
            "clobbers": [
                "navigator.globalization"
            ]
        }
    ]
});